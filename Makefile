DEVICE := odin

MODEM_IMAGE := firmware-update/NON-HLOS.bin
SBL1_MBN := firmware-update/sbl1.mbn
APPSBOOT_MBN := firmware-update/emmc_appsboot.mbn

TIMESTAMP := $(shell strings $(MODEM_IMAGE) | sed -n 's/.*"Time_Stamp": "\([^"]*\)"/\1/p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's/[ :-]*//g')

HASH_SBL1 := $(shell openssl dgst -r -sha1 $(SBL1_MBN) | cut -d ' ' -f 1)
HASH_APPSBOOT := $(shell openssl dgst -r -sha1 $(APPSBOOT_MBN) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

$(TARGET): META-INF firmware-update
	zip -r9 $@ $^

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(SBL1_MBN) $(APPSBOOT_MBN)
ifneq ($(HASH_SBL1), 9534d29f0ee14d04fbb1938fb5bfaa362b2e7512)
	$(error SHA-1 of sbl1.mbn mismatch)
endif
ifneq ($(HASH_APPSBOOT), 8965d41a1de44fa3aea71867366d787b72cd2380)
	$(error SHA-1 of emmc_appsboot.mbn mismatch)
endif

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMAGE)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
